#ifndef vCard_H
#define vCard_H

#include "vCardN.h"
#include "vCardFN.h"
#include "vCardTEL.h"
#include "vCardEMAIL.h"
#include "vCardORG.h"
#include "vCardTITLE.h"

#include <string>
#include <vector>
#include <set>
#include <ostream>

class vCard
{

    vCardN *name = nullptr;
    vCardFN *full_name = nullptr;
    std::vector<vCardTEL> phones;
    std::vector<vCardEMAIL> emails;
    vCardORG *org = nullptr;
    vCardTITLE *title = nullptr;

public:
    static std::vector<vCard> extract_vCards(const std::vector<std::string> raw_data);

    static void sort(std::vector<vCard> &vCards);

    static bool find_duplicates(const std::vector<vCard> &vCards, std::set<vCard> &duplicates);

    static bool find_duplicates_by_name(const std::vector<vCard> &vCards, std::set<vCard> &duplicates);

    static bool find_duplicates_by_number(const std::vector<vCard> &vCards, std::set<vCard> &duplicates);
    

    vCard(const std::vector<std::string> raw_data) noexcept(false);

    friend std::ostream &operator<<(std::ostream &os, const vCard &obj);

    friend bool operator<(const vCard &lhs, const vCard &rhs);
    friend bool operator>(const vCard &lhs, const vCard &rhs);
    friend bool operator<=(const vCard &lhs, const vCard &rhs);
    friend bool operator>=(const vCard &lhs, const vCard &rhs);

    friend bool operator==(const vCard &lhs, const vCard &rhs);
    friend bool operator!=(const vCard &lhs, const vCard &rhs);
};

#endif