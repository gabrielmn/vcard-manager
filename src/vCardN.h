#ifndef vCard_N_H
#define vCard_N_H

#include "vCardFN.h"

#include <string>
#include <ostream>

class vCardN
{

    std::string last_name;
    std::string first_name;
    std::string middle_name;
    std::string prefix;
    std::string suffix;

public:

    vCardN(){};
    vCardN(const std::string raw_data) noexcept(false);

    vCardFN generate_vCardFN();

    friend std::ostream &operator<<(std::ostream &os, const vCardN &obj);

    friend bool operator<(const vCardN &lhs, const vCardN &rhs);
    friend bool operator>(const vCardN &lhs, const vCardN &rhs);
    friend bool operator<=(const vCardN &lhs, const vCardN &rhs);
    friend bool operator>=(const vCardN &lhs, const vCardN &rhs);

    friend bool operator==(const vCardN &lhs, const vCardN &rhs);
    friend bool operator!=(const vCardN &lhs, const vCardN &rhs);
};

#endif