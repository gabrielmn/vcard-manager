#ifndef vCard_TEL_H
#define vCard_TEL_H

#include <string>
#include <vector>
#include <ostream>

enum class vCardTEL_Type
{
    CELL,
    HOME,
    WORK,
    PAGER,
    FAX,
    PREF
};

class vCardTEL
{

    std::vector<vCardTEL_Type> types;
    std::string number;

public:
 
    vCardTEL(const std::string raw_data) noexcept(false);

    friend std::ostream &operator<<(std::ostream &os, const vCardTEL &obj);
    friend bool operator==(const vCardTEL &lhs, const vCardTEL &rhs);
    friend bool operator!=(const vCardTEL &lhs, const vCardTEL &rhs);

    std::string get_number() const;
};

#endif