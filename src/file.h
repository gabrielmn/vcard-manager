#ifndef FILE_H
#define FILE_H
#include <filesystem>
#include <string>
#include <vector>

class file
{

    std::filesystem::path path;
    
public:
    
    /**
     * @brief Check if the file with the name exists in the current directory.
     * 
     * @param name file name
     * @return true if the file exists
     * @return false if the file don't exists
     */
    static bool file_exist(const std::string name);

    /**
     * @brief Check if the file with the name exists in the path.
     * 
     * @param name file name
     * @param path file path
     * @return true if the file exists
     * @return false if the file don't exists
     */
    static bool file_exist(const std::string name, const std::string path);

    /**
     * @brief Construct a new file object
     * 
     * @param name 
     * @param path 
     * @param create 
     */
    file(const std::string name, const std::string path, const bool create = false)noexcept(false);

    /**
     * @brief Construct a new file object
     * 
     * @param name 
     * @param create 
     */
    file(const std::string name, const bool create = false) noexcept(false);
    
    /**
     * @brief Copy the current file to another location.
     * 
     * @param to location to where the file will be copied.
     * @return true if the file was copied.
     * @return false if the file wasn't copied.
     */
    bool copy(const std::filesystem::path to);

    /**
     * @brief Check if the file exists.
     * 
     * @return true if the file exists.
     * @return false if the file don't exist.
     */
    bool exist();
    
    /**
     * @brief Check if the file path exists. 
     * 
     * @return true if the file path exists.
     * @return false if the file path don't exist.
     */
    bool exist_path();

    /**
     * @brief Get the name of the file.
     * 
     * @return std::string 
     */
    std::string get_name();
    
    /**
     * @brief Get the path of the file.
     * 
     * @return std::string 
     */
    std::string get_path();
    
    /**
     * @brief Read all lines in the file.
     * 
     * @return std::vector<std::string> 
     */
    std::vector<std::string> read();

    /**
     * @brief Delete the current file.
     * 
     * @return true if the file was deleted.
     * @return false if the file wasn't deleted.
     */
    bool remove();

    /**
     * @brief Write lines to the files
     * 
     * @param lines 
     * @param overwrite 
     */
    void write(const std::vector<std::string> &lines, const bool overwrite = false);

    /**
     * @brief Write line to the file
     * 
     * @param line 
     * @param overwrite 
     */
    void write_line(const std::string &line, const bool overwrite = false);

    
};

#endif