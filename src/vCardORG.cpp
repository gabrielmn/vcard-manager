#include "vCardORG.h"

vCardORG::vCardORG(const std::string raw_data) noexcept(false)
{
    if ("ORG:" != raw_data.substr(0, 4))
    {
        throw std::invalid_argument("not a vCardORG");
    }
    this->org = raw_data.substr(4);
}

std::ostream &operator<<(std::ostream &os, const vCardORG &obj)
{
    os << "ORG:"
       << obj.org;
    return os;
}

bool operator==(const vCardORG &lhs, const vCardORG &rhs)
{
    return (lhs.org == rhs.org);
}

bool operator!=(const vCardORG &lhs, const vCardORG &rhs)
{
    return !( lhs == rhs);
}