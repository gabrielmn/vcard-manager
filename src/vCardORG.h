#ifndef vCard_ORG_H
#define vCard_ORG_H

#include <string>
#include <ostream>

class vCardORG
{

    std::string org;

public:
 
    vCardORG(const std::string raw_data) noexcept(false);

    friend std::ostream &operator<<(std::ostream &os, const vCardORG &obj);

    friend bool operator==(const vCardORG &lhs, const vCardORG &rhs);
    friend bool operator!=(const vCardORG &lhs, const vCardORG &rhs);
};

#endif