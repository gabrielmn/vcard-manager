#ifndef vCard_TITLE_H
#define vCard_TITLE_H

#include <string>
#include <ostream>

class vCardTITLE
{

    std::string title;

public:
 
    vCardTITLE(const std::string raw_data) noexcept(false);

    friend std::ostream &operator<<(std::ostream &os, const vCardTITLE &obj);

    friend bool operator==(const vCardTITLE &lhs, const vCardTITLE &rhs);
    friend bool operator!=(const vCardTITLE &lhs, const vCardTITLE &rhs);
};

#endif