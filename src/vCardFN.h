#ifndef vCard_FN_H
#define vCard_FN_H

#include <string>
#include <ostream>

class vCardFN
{

    std::string full_name;

public:

    vCardFN(){};
    vCardFN(const std::string raw_data) noexcept(false);

    vCardFN(const std::string last_name, const std::string first_name, const std::string middle_name, const std::string prefix, const std::string suffix);

    friend std::ostream &operator<<(std::ostream &os, const vCardFN &obj);

    friend bool operator==(const vCardFN &lhs, const vCardFN &rhs);
    friend bool operator!=(const vCardFN &lhs, const vCardFN &rhs);
};

#endif