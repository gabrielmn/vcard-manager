#include "vCardEMAIL.h"

vCardEMAIL::vCardEMAIL(const std::string raw_data) noexcept(false)
{
    if ("EMAIL" != raw_data.substr(0, 5))
    {
        throw std::invalid_argument("not a vCardEMAIL");
    }
    if (':' == raw_data[5])
    {
        this->email = raw_data.substr(6);
    }
    else if (';' == raw_data[5])
    {
        std::size_t start = 4;
        do
        {
            start += 1;
            std::size_t end = raw_data.find(";", start);
            if (end == std::string::npos)
            {
                end = raw_data.find(":", start);
            }
            std::string type = raw_data.substr(start, end - start);
            if (type == "HOME")
            {
                this->types.push_back(vCardEMAIL_Type::HOME);
            }
            else if (type == "WORK")
            {
                this->types.push_back(vCardEMAIL_Type::WORK);
            }
            else if (type == "PREF")
            {
                this->types.push_back(vCardEMAIL_Type::PREF);
            }
            start = end;
        } while (raw_data[start] != ':');

        this->email = raw_data.substr(start + 1);
    }
}

std::ostream &operator<<(std::ostream &os, const vCardEMAIL &obj)
{
    os << "EMAIL";
    if (!obj.types.empty())
    {
        for (std::size_t index = 0; index < obj.types.size(); index++)
        {
            if (obj.types[index] == vCardEMAIL_Type::HOME)
            {
                os << ";HOME";
            }
            else if (obj.types[index] == vCardEMAIL_Type::WORK)
            {
                os << ";WORK";
            }
            else if (obj.types[index] == vCardEMAIL_Type::PREF)
            {
                os << ";PREF";
            }
        }
    }
    os << ":"
       << obj.email;
    return os;
}

bool operator==(const vCardEMAIL &lhs, const vCardEMAIL &rhs)
{
    if (lhs.email != rhs.email)
        return false;

    if (lhs.types.size() != rhs.types.size())
        return false;

    for (std::size_t index = 0; index < lhs.types.size(); index++)
        if (lhs.types[index] != rhs.types[index])
            return false;

    return true;
}

bool operator!=(const vCardEMAIL &lhs, const vCardEMAIL &rhs)
{
    return !(lhs == rhs);   
}