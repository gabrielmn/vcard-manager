#ifndef vCard_EMAIL_H
#define vCard_EMAIL_H

#include <string>
#include <vector>
#include <ostream>

enum class vCardEMAIL_Type
{
    HOME,
    WORK,
    PREF
};

class vCardEMAIL
{

    std::vector<vCardEMAIL_Type> types;
    std::string email;

public:
  
    vCardEMAIL(const std::string raw_data) noexcept(false);

    friend std::ostream &operator<<(std::ostream &os, const vCardEMAIL &obj);

    friend bool operator==(const vCardEMAIL &lhs, const vCardEMAIL &rhs);
    friend bool operator!=(const vCardEMAIL &lhs, const vCardEMAIL &rhs);
};

#endif