#include "vCardN.h"
#include <stdexcept>
#include <vector>

#include <iostream>

vCardN::vCardN(const std::string raw_data) noexcept(false)
{
    if ("N:" != raw_data.substr(0, 2))
    {
        throw std::invalid_argument("not a vCardN");
    }

    std::vector<std::string> temp_split;

    std::size_t start = 1;
    do
    {
        start += 1;
        std::size_t end = raw_data.find(";", start);
        temp_split.push_back(raw_data.substr(start, end - start));
        start = end;
    } while (start != std::string::npos);

    if (temp_split.size() > 5)
    {
        throw std::invalid_argument("too many arguments, not a vCardN");
    }

    this->last_name = temp_split[0];
    this->first_name = temp_split[1];
    this->middle_name = temp_split[2];
    this->prefix = temp_split[3];
    this->suffix = temp_split[4];
}

vCardFN vCardN::generate_vCardFN()
{
    return vCardFN(this->last_name, this->first_name, this->middle_name, this->prefix, this->suffix);
}

std::ostream &operator<<(std::ostream &os, const vCardN &obj)
{
    os << "N:"
       << obj.last_name
       << ";"
       << obj.first_name
       << ";"
       << obj.middle_name
       << ";"
       << obj.prefix
       << ";"
       << obj.suffix;
    return os;
}

bool operator<(const vCardN &lhs, const vCardN &rhs)
{
    std::string str_lhs = lhs.first_name + " " + lhs.middle_name + " " + lhs.last_name;

    std::string str_rhs = rhs.first_name + " " + rhs.middle_name + " " + rhs.last_name;

    return str_lhs < str_rhs;
}

bool operator>(const vCardN &lhs, const vCardN &rhs)
{
    return rhs < lhs;
}

bool operator<=(const vCardN &lhs, const vCardN &rhs)
{
    return !(lhs > rhs);
}

bool operator>=(const vCardN &lhs, const vCardN &rhs)
{
    return !(lhs < rhs);
}

bool operator==(const vCardN &lhs, const vCardN &rhs)
{
    return (
        lhs.first_name == rhs.first_name &&
        lhs.middle_name == rhs.middle_name &&
        lhs.last_name == rhs.last_name &&
        lhs.prefix == rhs.prefix &&
        lhs.suffix == rhs.suffix
    );
}

bool operator!=(const vCardN &lhs, const vCardN &rhs)
{
    return !(lhs == rhs);
}