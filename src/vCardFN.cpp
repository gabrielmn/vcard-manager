#include "vCardFN.h"

vCardFN::vCardFN(const std::string raw_data) noexcept(false)
{
    if ("FN:" != raw_data.substr(0, 3))
    {
        throw std::invalid_argument("not a vCardFN");
    }
    this->full_name = raw_data.substr(3);
}

vCardFN::vCardFN(const std::string last_name, const std::string first_name, const std::string middle_name, const std::string prefix, const std::string suffix)
{
    this->full_name += prefix;
    this->full_name += " ";
    this->full_name += first_name;
    this->full_name += " ";
    this->full_name += middle_name;
    this->full_name += " ";
    this->full_name += last_name;
    this->full_name += ",";
    this->full_name += suffix;
}

std::ostream &operator<<(std::ostream &os, const vCardFN &obj)
{
    os << "FN:"
       << obj.full_name;
    return os;
}

bool operator==(const vCardFN &lhs, const vCardFN &rhs)
{
    return lhs.full_name == rhs.full_name;
}

bool operator!=(const vCardFN &lhs, const vCardFN &rhs)
{
    return !(lhs == rhs);
}