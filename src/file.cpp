#include "file.h"
#include <fstream>

bool file::file_exist(const std::string name)
{
    return file::file_exist(name, std::filesystem::current_path().string());
}

bool file::file_exist(const std::string name, const std::string path)
{
    return std::filesystem::exists(path + std::filesystem::path::preferred_separator + name);
}

file::file(const std::string name, const std::string path, const bool create)
{

    this->path = std::filesystem::path(path);
    this->path += std::filesystem::path::preferred_separator;
    this->path += name;

    if (create && !std::filesystem::exists(this->path))
    {
        if (!std::filesystem::exists(this->path.parent_path().c_str()))
        {
            std::filesystem::create_directories(this->path.parent_path().c_str());
        }

        std::fstream file(this->path.c_str(), std::ios::out);
        if (!file.is_open())
        {
            throw std::string("can't create file");
        }
        file.close();
    }
}

file::file(const std::string name, const bool create)
{
#ifdef __unix__
    *this = file(name, std::filesystem::current_path(), create);
#elif __WIN32
    this->path = std::filesystem::current_path();
    this->path += std::filesystem::path::preferred_separator;
    this->path += name;

    if (create && !std::filesystem::exists(this->path))
    {
        if (!std::filesystem::exists(this->path.parent_path().c_str()))
        {
            std::filesystem::create_directories(this->path.parent_path().c_str());
        }

        std::fstream file(this->path.c_str(), std::ios::out);
        if (!file.is_open())
        {
            throw std::string("can't create file");
        }
        file.close();
    }
#endif
}

bool file::copy(std::filesystem::path to)
{
    if (this->exist())
        return std::filesystem::copy_file(this->path, to);
    return false;
}

bool file::exist()
{
    return std::filesystem::exists(this->path);
}

bool file::exist_path()
{
    return std::filesystem::exists(this->path.parent_path().c_str());
}

std::string file::get_name()
{
    return this->path.filename().string();
}

std::string file::get_path()
{
    return this->path.parent_path().string();
}

std::vector<std::string> file::read()
{
    std::fstream file(this->path.c_str(), std::ios::in);
    if (!file.is_open())
        throw std::string("can't open file");

    std::vector<std::string> temp;

    for(std::string line; std::getline(file, line); )
        temp.push_back(line);

    return temp;
}

bool file::remove()
{
    if (this->exist())
        return std::filesystem::remove(this->path);
    return false;
}

void file::write(const std::vector<std::string> &lines, const bool overwrite)
{
    std::fstream file(this->path.c_str(), std::ios::out | ((overwrite) ? std::ios::trunc : std::ios::app));
    if (!file.is_open())
        throw std::string("can't open file");
    
    for(std::size_t index = 0; index < lines.size(); index++)
        file << lines[index] <<std::endl;

    file.close();
}

void file::write_line(const std::string &line, const bool overwrite)
{
    std::fstream file(this->path.c_str(), std::ios::out | ((overwrite) ? std::ios::trunc : std::ios::app));
    if (!file.is_open())
        throw std::string("can't open file");
    file << line << std::endl;
    file.close();
}