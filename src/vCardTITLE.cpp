#include "vCardTITLE.h"

vCardTITLE::vCardTITLE(const std::string raw_data) noexcept(false)
{
    if ("TITLE:" != raw_data.substr(0, 6))
    {
        throw std::invalid_argument("not a vCardTITLE");
    }
    this->title = raw_data.substr(6);
}

std::ostream &operator<<(std::ostream &os, const vCardTITLE &obj)
{
    os << "TITLE:"
       << obj.title;
    return os;
}

bool operator==(const vCardTITLE &lhs, const vCardTITLE &rhs)
{
    return (lhs.title == rhs.title);
}

bool operator!=(const vCardTITLE &lhs, const vCardTITLE &rhs)
{
    return !( lhs == rhs);
}