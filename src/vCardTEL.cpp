#include "vCardTEL.h"
#include <iostream>

vCardTEL::vCardTEL(const std::string raw_data) noexcept(false)
{
    if ("TEL" != raw_data.substr(0, 3))
    {
        throw std::invalid_argument("not a vCardTEL");
    }
    if (':' == raw_data[3])
    {
        this->number = raw_data.substr(4);
    }
    else if (';' == raw_data[3])
    {
        std::size_t start = 3;
        do
        {
            start += 1;
            std::size_t end = raw_data.find(";", start);
            if (end == std::string::npos)
            {
                end = raw_data.find(":", start);
            }
            std::string type = raw_data.substr(start, end - start);
            if (type == "CELL")
            {
                this->types.push_back(vCardTEL_Type::CELL);
            }
            else if (type == "HOME")
            {
                this->types.push_back(vCardTEL_Type::HOME);
            }
            else if (type == "WORK")
            {
                this->types.push_back(vCardTEL_Type::WORK);
            }
            else if (type == "PAGER")
            {
                this->types.push_back(vCardTEL_Type::PAGER);
            }
            else if (type == "FAX")
            {
                this->types.push_back(vCardTEL_Type::FAX);
            }
            else if (type == "PREF")
            {
                this->types.push_back(vCardTEL_Type::PREF);
            }
            start = end;
        } while (raw_data[start] != ':');

        this->number = raw_data.substr(start + 1);
    }
}

std::ostream &operator<<(std::ostream &os, const vCardTEL &obj)
{
    os << "TEL";
    if (!obj.types.empty())
    {
        for (std::size_t index = 0; index < obj.types.size(); index++)
        {
            if (obj.types[index] == vCardTEL_Type::CELL)
            {
                os << ";CELL";
            }
            else if (obj.types[index] == vCardTEL_Type::HOME)
            {
                os << ";HOME";
            }
            else if (obj.types[index] == vCardTEL_Type::WORK)
            {
                os << ";WORK";
            }
            else if (obj.types[index] == vCardTEL_Type::PAGER)
            {
                os << ";PAGER";
            }
            else if (obj.types[index] == vCardTEL_Type::FAX)
            {
                os << ";FAX";
            }
            else if (obj.types[index] == vCardTEL_Type::PREF)
            {
                os << ";PREF";
            }
        }
    }
    os << ":"
       << obj.number;
    return os;
}

bool operator==(const vCardTEL &lhs, const vCardTEL &rhs)
{
    if (lhs.number != rhs.number)
        return false;

    if (lhs.types.size() != rhs.types.size())
        return false;

    for (std::size_t index = 0; index < lhs.types.size(); index++)
        if (lhs.types[index] != rhs.types[index])
            return false;

    return true;
}

bool operator!=(const vCardTEL &lhs, const vCardTEL &rhs)
{
    return !(lhs == rhs);
}

std::string vCardTEL::get_number() const
{
    return this->number;
}