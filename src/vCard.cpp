#include "vCard.h"
#include <algorithm>

std::vector<vCard> vCard::extract_vCards(const std::vector<std::string> raw_data)
{
    std::vector<vCard> temp_vCards;

    for (std::vector<std::string>::const_iterator it = raw_data.begin(); it != raw_data.end(); it++)
    {
        if (*it == "BEGIN:VCARD")
        {
            std::vector<std::string> temp;
            for (; it != raw_data.end(); it++)
            {
                temp.push_back(*it);
                if (*it == "END:VCARD")
                {
                    break;
                }
            }
            temp_vCards.push_back(vCard(temp));
        }
    }

    return temp_vCards;
}

void vCard::sort(std::vector<vCard> &vCards)
{
    std::sort(vCards.begin(), vCards.end());
}

bool vCard::find_duplicates(const std::vector<vCard> &vCards, std::set<vCard> &duplicates)
{
    duplicates.clear();
    for (auto it_i = vCards.begin(); it_i != vCards.end(); ++it_i)
    {
        for (auto it_j = it_i + 1; it_j != vCards.end(); ++it_j)
        {
            if (*it_i == *it_j)
                duplicates.insert(*it_i);
        }
    }

    return (duplicates.size() > 0);
}

bool vCard::find_duplicates_by_name(const std::vector<vCard> &vCards, std::set<vCard> &duplicates)
{
    duplicates.clear();
    for (auto it_i = vCards.begin(); it_i != vCards.end(); ++it_i)
    {
        for (auto it_j = it_i + 1; it_j != vCards.end(); ++it_j)
        {
            if ((*it_i).name == (*it_j).name)
                duplicates.insert(*it_i);
        }
    }

    return (duplicates.size() > 0);
}

bool vCard::find_duplicates_by_number(const std::vector<vCard> &vCards, std::set<vCard> &duplicates)
{
    duplicates.clear();
    for (auto it_i = vCards.begin(); it_i != vCards.end(); ++it_i)
    {
        for (auto it_j = it_i + 1; it_j != vCards.end(); ++it_j)
        {
            for (auto it_n = (*it_i).phones.begin(); it_n != (*it_i).phones.end(); ++it_n)
            {
                for (auto it_m = (*it_j).phones.begin(); it_m != (*it_j).phones.end(); ++it_m)
                {
                    if( (*it_n).get_number() == (*it_m).get_number())
                        duplicates.insert(*it_i);        
                }
            }
        }
    }

    return (duplicates.size() > 0);
}

vCard::vCard(const std::vector<std::string> raw_data) noexcept(false)
{
    if (*raw_data.begin() != "BEGIN:VCARD" || *(raw_data.begin() + 1) != "VERSION:2.1" || *raw_data.rbegin() != "END:VCARD")
    {
        throw std::invalid_argument("not a vCard");
    }

    for (std::size_t index = 2; index < raw_data.size() - 1; index++)
    {
        if (raw_data[index].substr(0, 2) == "N:")
        {
            if (this->name == nullptr)
            {
                this->name = new vCardN(raw_data[index]);
            }
            else
            {
                throw std::invalid_argument("too many vCardN");
            }
        }
        else if (raw_data[index].substr(0, 3) == "FN:")
        {
            if (this->full_name == nullptr)
            {
                this->full_name = new vCardFN(raw_data[index]);
            }
            else
            {
                throw std::invalid_argument("too many vCardFN");
            }
        }
        else if (raw_data[index].substr(0, 3) == "TEL")
        {
            this->phones.push_back(vCardTEL(raw_data[index]));
        }
        else if (raw_data[index].substr(0, 5) == "EMAIL")
        {
            this->emails.push_back(vCardEMAIL(raw_data[index]));
        }
        else if (raw_data[index].substr(0, 4) == "ORG:")
        {
            if (this->org == nullptr)
            {
                this->org = new vCardORG(raw_data[index]);
            }
            else
            {
                throw std::invalid_argument("too many vCardORG");
            }
        }
        else if (raw_data[index].substr(0, 6) == "TITLE:")
        {
            if (this->title == nullptr)
            {
                this->title = new vCardTITLE(raw_data[index]);
            }
            else
            {
                throw std::invalid_argument("too many vCardTITLE");
            }
        }
    }
}

std::ostream &operator<<(std::ostream &os, const vCard &obj)
{
    os << "BEGIN:VCARD" << std::endl
       << "VERSION:2.1" << std::endl;

    if (obj.name != nullptr)
        os << *obj.name << std::endl;

    if (obj.full_name != nullptr)
        os << *obj.full_name << std::endl;

    for (std::size_t index = 0; index < obj.phones.size(); index++)
        os << obj.phones[index] << std::endl;

    for (std::size_t index = 0; index < obj.emails.size(); index++)
        os << obj.emails[index] << std::endl;

    if (obj.org != nullptr)
        os << *obj.org << std::endl;

    if (obj.title != nullptr)
        os << *obj.title << std::endl;

    os << "END:VCARD" << std::endl;
    return os;
}

bool operator<(const vCard &lhs, const vCard &rhs)
{
    return *lhs.name < *rhs.name;
}

bool operator>(const vCard &lhs, const vCard &rhs)
{
    return rhs < lhs;
}

bool operator<=(const vCard &lhs, const vCard &rhs)
{
    return !(lhs > rhs);
}

bool operator>=(const vCard &lhs, const vCard &rhs)
{
    return !(lhs < rhs);
}

bool operator==(const vCard &lhs, const vCard &rhs)
{
    if ((lhs.name == nullptr && rhs.name == nullptr) ||
        (lhs.name == nullptr && rhs.name != nullptr) ||
        (lhs.name != nullptr && rhs.name == nullptr) ||
        (*lhs.name != *rhs.name))
        return false;

    if ((lhs.full_name == nullptr && rhs.full_name == nullptr) ||
        (lhs.full_name == nullptr && rhs.full_name != nullptr) ||
        (lhs.full_name != nullptr && rhs.full_name == nullptr) ||
        (*lhs.full_name != *rhs.full_name))
        return false;

    if ((lhs.org == nullptr && rhs.org == nullptr) ||
        (lhs.org == nullptr && rhs.org != nullptr) ||
        (lhs.org != nullptr && rhs.org == nullptr) ||
        (*lhs.org != *rhs.org))
        return false;

    if ((lhs.title == nullptr && rhs.title == nullptr) ||
        (lhs.title == nullptr && rhs.title != nullptr) ||
        (lhs.title != nullptr && rhs.title == nullptr) ||
        (*lhs.title != *rhs.title))
        return false;

    if (lhs.phones.size() != rhs.phones.size())
        return false;

    if (lhs.emails.size() != rhs.emails.size())
        return false;

    for (std::size_t index = 0; index < lhs.phones.size(); index++)
        if (lhs.phones[index] != rhs.phones[index])
            return false;

    for (std::size_t index = 0; index < lhs.emails.size(); index++)
        if (lhs.emails[index] != rhs.emails[index])
            return false;

    return true;
}

bool operator!=(const vCard &lhs, const vCard &rhs)
{
    return !(lhs == rhs);
}